# Compatibility module to provide ibisclient at the same location as when we
# vendored it.
from ibisclient import *  # noqa: F401, F403
