# AutoDevOps-friendly Dockerfile. This Dockerfile is simply used to build a
# container containing the code so that automated container scanning processes
# can work. As such it doesn't really need to allow the code to be "run", just
# be present. Despite this we do a "pip install" so that we test the
# pip-install-ability of the library
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:latest

WORKDIR /usr/src/app

ADD ./ ./

RUN pip install .
